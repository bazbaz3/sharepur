package app.sharepur.utils

//import com.bindhupublications.BR
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.sharepur.BR
import kotlin.collections.ArrayList

open class BaseListAdapter<T> : ListAdapter<T, BaseListHolder<T>> {

    internal var mEmptyTextView: TextView? = null
    internal var mEmptyImageView: View? = null
    internal var resLayoutID: Int = 0
    var onListItemClickListener: OnListItemClickListener<T>? = null

    constructor(resLayoutID: Int) : super(BaseDiffUtils<T>(null, ArrayList<T>())) {
        this.resLayoutID = resLayoutID
    }
    constructor(resLayoutID: Int,list : ArrayList<T>) : super(BaseDiffUtils<T>(null, ArrayList<T>())) {
        this.resLayoutID = resLayoutID
        this.submitList(list)
    }

    constructor(diffUtil: DiffUtil.ItemCallback<T>) : super(diffUtil) {}

    constructor(diffUtil: DiffUtil.ItemCallback<T>, resLayoutID: Int) : super(diffUtil) {
        this.resLayoutID = resLayoutID
    }

    fun setOnItemClickListener(clickListener: OnListItemClickListener<T>): BaseListAdapter<T> {
        this.onListItemClickListener = clickListener
        notifyDataSetChanged()
        return this
    }

    fun setEmptyTextView(mEmptyTextView: TextView) {
        this.mEmptyTextView = mEmptyTextView
    }

    fun setEmptyImageView(mEmptyImageView: View) {
        this.mEmptyImageView = mEmptyImageView
    }

    fun setList(list: ArrayList<T>?): BaseListAdapter<T> {
        var list = list
        if (list == null)
            list = ArrayList()
        submitList(ArrayList(list))
        notifyDataSetChanged()
        return this
    }

/*
    fun setList(list: List<T>): BaseListAdapter<T> {
        submitList(ArrayList(list))
        notifyDataSetChanged()
        return this
    }
*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseListHolder<T> {
        return BaseListHolder(
            DataBindingUtil.inflate<ViewDataBinding>(
                LayoutInflater.from(parent.context),
                viewType,
                parent,
                false
            )
        )
    }

    override fun getItemViewType(position: Int): Int {
        return resLayoutID
    }

    override fun getItemCount(): Int {
        if (mEmptyTextView != null) {
            if (super.getItemCount() <= 0) {
                mEmptyTextView!!.visibility = View.VISIBLE
                if (mEmptyImageView != null)
                    mEmptyImageView!!.visibility = View.VISIBLE
            } else {
                mEmptyTextView!!.visibility = View.GONE
                if (mEmptyImageView != null)
                    mEmptyImageView!!.visibility = View.GONE
            }
        }
        return super.getItemCount()
    }

    override fun onBindViewHolder(holder: BaseListHolder<T>, position: Int) {
        holder.bind(getItem(position))
        holder.binding.setVariable(BR.clicks, onListItemClickListener)
    }

    interface OnListItemClickListener<T> {
        fun onItemClick(view: View, itemData: T, position: Int)
    }
}
