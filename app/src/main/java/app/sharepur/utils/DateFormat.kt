package app.sharepur.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateFormat {

    var FORMAT_MMM_DD = SimpleDateFormat("MMM dd", Locale.getDefault())
    var FORMAT_HH_MM_SS = SimpleDateFormat("HH:mm:ss", Locale.getDefault())//09:00 PM
    var FORMAT_HH_MM_A = SimpleDateFormat("hh:mm a", Locale.getDefault())//09:00 PM
    var FORMAT_HHC_MM_A = SimpleDateFormat("HH:mm a", Locale.getDefault())//09:00 PM
    var FORMAT_HH_MM = SimpleDateFormat("HH:mm", Locale.getDefault())//09:00 PM
    var FORMAT_DD_MM_YY = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())//09:00 PM
    var FORMAT_YYYY_MM_DD__HH_MM_SS =
        SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())//09:00 PM
    var FORMAT_YYYY_MM_DD = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())//09:00 PM
    var FORMAT_YYYY = SimpleDateFormat("yyyy", Locale.getDefault())//09:00 PM
    var FORMAT_MM = SimpleDateFormat("MM", Locale.getDefault())//09:00 PM
    var FORMAT_MMM_DD_YYYY__HH_MM_A =
        SimpleDateFormat("MMM dd yyyy hh:mm a", Locale.getDefault())//09:00 PM
    var FORMAT_HH_MM_A_DD_MMM_YYYY =
        SimpleDateFormat("hh:mm a, dd MMM, yyyy", Locale.getDefault())//09:09 AM, 05 Jul, 2019
    var FORMAT_MMM_DD_COMMA_HH_MM_A =
        SimpleDateFormat("MMM dd, hh:mm a", Locale.getDefault())//09:00 PM
    // public static SimpleDateFormat FORMAT_MMM_DD_COMMA_HH_MM_A = new SimpleDateFormat("MMM dd, hh:mm a", Locale.getDefault());//09:00 PM
    var FORMAT_MMMMM_DD = SimpleDateFormat("MMMM dd", Locale.getDefault())//09:00 PM
    var FORMAT_MMM_DDth = SimpleDateFormat("MMM dd", Locale.getDefault())//09:00 PM
    var FORMAT_EE = SimpleDateFormat("EE", Locale.getDefault())//09:00 PM
    var FORMAT_DD = SimpleDateFormat("dd", Locale.getDefault())//09:00 PM

    fun getDateStrFromFormattedStr(
        fromFormat: SimpleDateFormat,
        fromStr: String,
        returnFormat: SimpleDateFormat
    ): String? {
        try {
            return getDateStrFromDate(returnFormat, fromFormat.parse(fromStr))
        } catch (e: Exception) {
            e.printStackTrace()
            return fromStr
        }

    }

    fun getDateStrFromDate(format: SimpleDateFormat, dateToBeFormatted: Date?): String {
        return format.format(dateToBeFormatted!!)
    }

    fun getDateFromDateStr(fromFormat: SimpleDateFormat, dateStr: String): Calendar {
        val cal = Calendar.getInstance()
        try {
            cal.time = fromFormat.parse(dateStr)!!
            return cal
        } catch (e: ParseException) {
            e.printStackTrace()
            return cal
        }

    }
}
