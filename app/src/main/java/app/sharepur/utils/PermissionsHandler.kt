package app.sharepur.utils;

import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

open class PermissionsHandler {

    internal var mActivity: FragmentActivity
    internal var mFragment: Fragment? = null
    internal var permissionListener: OnPermissionListener
    internal var permissions: Array<String>
    private var permissionError: Int = 0

    constructor(
        mActivity: FragmentActivity,
        permissionListener: OnPermissionListener,
        permissions: Array<String>,
        permissionError: Int
    ) {
        this.mActivity = mActivity
        this.permissionListener = permissionListener
        this.permissions = permissions
        this.permissionError = permissionError
    }

    constructor(
        mFragment: Fragment,
        permissionListener: OnPermissionListener,
        permissions: Array<String>,
        permissionError: Int
    ) {
        this.mFragment = mFragment
        this.mActivity = mFragment.activity as FragmentActivity
        this.permissionListener = permissionListener
        this.permissions = permissions
        this.permissionError = permissionError
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (havePermissions()) {
                permissionListener.onPermissionGranted()
            } else {
                if (mFragment != null)
                    mFragment!!.requestPermissions(permissions, PERMISSION_REQUEST_CODE)
                else
                    mActivity.requestPermissions(permissions, PERMISSION_REQUEST_CODE)
            }
        } else {
            permissionListener.onPermissionGranted()
        }
    }

    fun onPermissionRequestResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (allPermissionsGiven(grantResults)) {
            permissionListener.onPermissionGranted()
        } else {
            showToast(mActivity, permissionError, null)
            permissionListener.onPermissionDenied()
        }
    }

    private fun allPermissionsGiven(grantResults: IntArray): Boolean {
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED)
                return false
        }
        return true
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun havePermissions(): Boolean {
        for (permission in permissions) {
            if (mActivity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    interface OnPermissionListener {
        fun onPermissionGranted()

        fun onPermissionDenied()
    }

    companion object {
        private val PERMISSION_REQUEST_CODE = 18
    }
}
