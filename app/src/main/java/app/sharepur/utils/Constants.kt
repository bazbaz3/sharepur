package app.sharepur.utils

class Constants {

    companion object{
        var EXTENTION_IMAGES = arrayListOf(".png",".tif", ".tiff",".bmp",".jpg", ".jpeg",".gif",".eps",".raw",".cr2",".net",".orf",".sr2")
        var EXTENTION_VIDEOS = arrayListOf(
            ".mkv",
            ".mp4",
            ".webm",
            ".flv",
            ".vob",
            ".ogv",
            ".ogg",
            ".drc",
            ".avi",
            ".mov",
            ".wmv",
            ".yuv",
            ".rm",
            ".m4p",
            ".m4v",
            ".3pg",
            ".3p2"
            )
        var EXTENTION_AUDIOS = arrayListOf(
            ".aa",
            ".aac",
            ".aax",
            ".act",
            ".aiff",
            ".alac",
            ".amr",
            ".ape",
            ".au",
            ".awb",
            ".dct",
            ".dss",
            ".flac",
            ".m4a",
            ".m4b",
            ".m4p",
            ".mmf",
            ".mp3",
            ".mpc",
            ".ogg",
            ".oga",
            ".mogg",
            ".wav",
            ".wma",
            ".vox",
            ".cda"
        )

        val seperator = "/"
    }
}