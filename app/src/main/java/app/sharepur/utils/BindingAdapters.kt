package app.sharepur.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter(value = ["setImageResId"])
fun setImageResId(imageView: ImageView, resID: Int) {
    imageView.setImageResource(resID)
}