package app.sharepur.utils

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData


fun showToast(context: Context, resStringID: Int? = 0, string: String? = "") {
    if (resStringID != null && resStringID != 0)
        Toast.makeText(context, resStringID, Toast.LENGTH_LONG).show()
    else
        Toast.makeText(context, string, Toast.LENGTH_LONG).show()
}

fun <T> MutableLiveData<ArrayList<T>>.update(data: T, add: Boolean) {
    val list = this.value as ArrayList
    if (add)
        list.add(data)
    else
        list.remove(data)
    this.value = list
}