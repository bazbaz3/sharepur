package app.sharepur.ui.receive

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import app.sharepur.R
import app.sharepur.utils.base.BaseFragment

class ReceiveFragment : BaseFragment() {

    private lateinit var receiveViewModel: ReceiveViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        receiveViewModel =
                ViewModelProvider(this).get(ReceiveViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_receive, container, false)
        return root
    }
}