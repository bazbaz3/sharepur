package app.sharepur.ui.send

import android.app.Application
import android.util.Log
import androidx.activity.OnBackPressedCallback
import androidx.lifecycle.MutableLiveData
import app.sharepur.R
import app.sharepur.models.FileInfo
import app.sharepur.models.FilePath
import app.sharepur.models.FileType
import app.sharepur.utils.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class SendViewModel(application: Application) : BaseViewModel(application) {

    val backCallback = object : OnBackPressedCallback(false) {
        override fun handleOnBackPressed() {
            gotoPreviousDirectory()
        }
    }

    val fileList = MutableLiveData<ArrayList<FileInfo>>()
    val pathList = MutableLiveData<ArrayList<FilePath>>()
    var currentOpenedDirectoryPath: String = ""

    init {
        fileList.value = ArrayList()
        pathList.value = ArrayList()
    }

    fun showFiles() {
        currentOpenedDirectoryPath = if (pathList.value == null || pathList.value.isNullOrEmpty()) {
            val path = StorageUtil.getStorageDirectories(getApplication())[0]
            pathList.update(FilePath(path, path), true)
            path
        } else {
            pathList.value!!.last().completePath
        }

        Log.e("Files Path", "Total Files $currentOpenedDirectoryPath")
        val rootFile = File(currentOpenedDirectoryPath)

        val fileInfoList = ArrayList<FileInfo>()
        if (rootFile.exists())
            rootFile.listFiles()?.forEach { file ->
                fileInfoList.add(FileInfo(file.absolutePath).also {
                    it.name = file.name
                    it.modified = file.lastModified()
                    it.isDirectory = file.isDirectory
                    it.description = if (it.isDirectory) {
                        String.format(
                            Locale.getDefault(),
                            getApplication<SharePurApp>().getString(R.string.s_files),
                            file.listFiles()!!.size
                        )
                    } else {
                        String.format(
                            Locale.getDefault(),
                            getApplication<SharePurApp>().getString(R.string.s_kb),
                            file.length() / 1000000
                        )
                    }
                    it.extension = file.extension
                    when {
                        Constants.EXTENTION_AUDIOS.contains(it.extension.toLowerCase(Locale.getDefault())) -> it.fileType =
                            FileType.AUDIO
                        Constants.EXTENTION_IMAGES.contains(it.extension.toLowerCase(Locale.getDefault())) -> it.fileType =
                            FileType.IMAGE
                        Constants.EXTENTION_VIDEOS.contains(it.extension.toLowerCase(Locale.getDefault())) -> it.fileType =
                            FileType.VIDEO
                        else -> it.fileType = FileType.UNKNOWN
                    }
                })
            }

        backCallback.isEnabled = pathList.value?.size!! > 1
        fileList.postValue(fileInfoList)
    }

    fun onItemClicked(itemData: FileInfo, position: Int) {
        if (itemData.isDirectory) {
            pathList.update(FilePath(itemData.name, itemData.path), true)
            showFiles()
        } else {

        }
    }

    fun onPathItemClicked(itemData: FilePath, position: Int) {
        if (currentOpenedDirectoryPath.endsWith(itemData.path))
            return
        currentOpenedDirectoryPath = ""
        pathList.value?.removeAll {
            it.completePath.length > itemData.completePath.length
        }
        pathList.postValue(pathList.value)
        showFiles()
    }

    private fun gotoPreviousDirectory() {
        val pathListCount = pathList.value?.size!! - 2
        onPathItemClicked(pathList.value?.get(pathListCount)!!, pathListCount)
    }

}