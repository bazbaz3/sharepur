package app.sharepur.ui.send

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import app.sharepur.R
import app.sharepur.databinding.FragmentSendBinding
import app.sharepur.models.FileInfo
import app.sharepur.models.FilePath
import app.sharepur.utils.BaseListAdapter
import app.sharepur.utils.PermissionsHandler
import app.sharepur.utils.base.BaseFragment

class SendFragment : BaseFragment(), PermissionsHandler.OnPermissionListener,
    BaseListAdapter.OnListItemClickListener<FileInfo> {

    private lateinit var sendViewModel: SendViewModel
    private lateinit var permissionHandler: PermissionsHandler

    private lateinit var adapter: BaseListAdapter<FileInfo>
    private lateinit var adapterPathList: BaseListAdapter<FilePath>
    private lateinit var binding: FragmentSendBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sendViewModel =
            ViewModelProvider(this).get(SendViewModel::class.java)

        sendViewModel.fileList.observe(this, Observer {
            if (binding.filesList.adapter == null) {
                adapter = BaseListAdapter(R.layout.item_list_files, it)
                binding.filesList.adapter = adapter
                adapter.setOnItemClickListener(this@SendFragment)
                binding.filesList.addItemDecoration(
                    DividerItemDecoration(
                        requireContext(),
                        RecyclerView.VERTICAL
                    )
                )
            }
            adapter.submitList(it)
            adapter.notifyDataSetChanged()
        })
        sendViewModel.pathList.observe(this, Observer {
            if (binding.pathList.adapter == null) {
                adapterPathList = BaseListAdapter(R.layout.item_list_path, it)
                binding.pathList.adapter =
                    adapterPathList.setOnItemClickListener(object :
                        BaseListAdapter.OnListItemClickListener<FilePath> {
                        override fun onItemClick(
                            view: View,
                            itemData: FilePath,
                            position: Int
                        ) {
                            sendViewModel.onPathItemClicked(itemData, position)
                        }
                    })
                binding.pathList.addItemDecoration(
                    DividerItemDecoration(
                        requireContext(),
                        RecyclerView.HORIZONTAL
                    )
                )
            }
            adapterPathList.submitList(it)
            adapterPathList.notifyDataSetChanged()
        })

        requireActivity().onBackPressedDispatcher.addCallback(sendViewModel.backCallback)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_send, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        permissionHandler = PermissionsHandler(
            this,
            this@SendFragment,
            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
            R.string.permission_storage_error
        )

        permissionHandler.checkPermissions()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionHandler.onPermissionRequestResult(requestCode, permissions, grantResults)
    }

    override fun onPermissionGranted() {
        sendViewModel.showFiles()
    }

    override fun onPermissionDenied() {

    }

    override fun onItemClick(view: View, itemData: FileInfo, position: Int) {
        sendViewModel.onItemClicked(itemData, position)
    }

}

