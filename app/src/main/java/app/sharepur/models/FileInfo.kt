package app.sharepur.models

import app.sharepur.R

class FileInfo(var path: String) {

    var name: String = ""
    var modified: Long = 0
    var extension: String = ""
    var isDirectory: Boolean = false
    var fileType = FileType.UNKNOWN
    var description = ""

    fun getFileIcon(): Int {
        if (isDirectory)
            return R.drawable.ic_folder_file

        return when (fileType) {
            FileType.AUDIO -> R.drawable.ic_audio_file
            FileType.VIDEO -> R.drawable.ic_video_file
            FileType.IMAGE -> R.drawable.ic_image_file
            else -> R.drawable.ic_other_file
        }
    }

}

enum class FileType { AUDIO, VIDEO, IMAGE, UNKNOWN }